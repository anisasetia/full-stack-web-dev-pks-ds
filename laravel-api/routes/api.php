<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

// Route::get('/post', 'PostController@index'); 
// Route::post('/post', 'PostController@store'); 
// Route::get('/post/{id}', 'PostController@show'); 
// Route::put('/post/{id}', 'PostController@update'); 
// Route::delete('/post/{id}', 'PostController@destroy'); 

Route::apiResource('post', 'PostController');
Route::apiResource('role', 'RoleController');
Route::apiResource('comment', 'CommentController');

Route::group([
    'prefix' => 'auth',
    'namespace' => 'Auth',

], function(){
   Route::post('register', 'RegisterController')->name('auth.register'); 
   Route::post('regenerate-otp-code', 'RegenerateOtpCodeController')->name('auth.regenerate-otp-code'); 
   Route::post('verification', 'VerificationController')->name('auth.verification'); 
   Route::post('update-password', 'UpdatePasswordController')->name('auth.update-password'); 
   Route::post('login', 'LoginController')->name('auth.login'); 
   
});

// Route::group([
//     'middleware' => 'auth:api'
// ], function () {
//     Route::apiResource('post', 'PostController@store');
//     Route::apiResource('post', 'PostController@update');
//     Route::apiResource('post', 'PostController@destroy');

//     Route::apiResource('comment', 'CommentController@store');
//     Route::apiResource('comment', 'CommentController@update');
//     Route::apiResource('comment', 'CommentController@destroy');
// });

