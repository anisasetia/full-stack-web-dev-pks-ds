<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->only('store','update','destroy');
    }

    public function index()
    {
        $posts = Post::latest()->get();

        if($posts){
            return response()->json([
                'success' => true,
                'message' => 'Lihat data post berhasil',
                'data' => $posts
            ]);
        }

        return response()->json([
            'success'   => false,
            'message' => 'Lihat data post gagal',
        ], 409);  
        
    }

    public function store(Request $request)
    {
        $allRequest = $request->all();
        $validator = Validator::make($allRequest , [
            'name' => 'required',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }

        // $user = auth()->user();

        $posts =  Post::create([
            'name' => $request->name,
            // 'user_id' => $user->id
        ]);

        if($posts){
            return response()->json([
                'success' => true,
                'message' => 'Masukkan data post berhasil',
                'data' => $posts,
            ], 200);
        }

        return response()->json([
            'success'   => false,
            'message' => 'Masukkan data post gagal',
        ], 409);  
    }

    public function show($id)
    {
        $posts = Post::find($id);

        if($posts){
            return response()->json([
                'success'   => true,
                'message'   => 'Tampilkan data berhasil',
                'data'      => $posts,
            ], 200);
        }

        return response()->json([
            'success'   => false,
            'message'   => 'Tampilkan data gagal',
            'data'      => $posts,
        ], 404);
        
    }

    public function update(Request $request, $id)
    {
        $allRequest = $request->all();
        $validator = Validator::make($allRequest , [
            'name' => 'required',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }

        $posts = Post::find($id);

        
        if($posts){
            $user = auth()->user();

            if($posts->user_id != $user->id)
            {
                return response()->json([
                    'success'   => false,
                    'message'   => 'Data post bukan milik user yang sedang login',
                ], 403);
            }

            $posts->update([
                'name' => $request->name,
            ]);

            return response()->json([
                'success'   => true,
                'message'   => 'Data dengan id '. $id .' berhasil di update',
                'data'      => $posts,
            ]);
        }
        
    }

    public function destroy($id)
    {
        $posts = Post::find($id);

        if($posts){

            $user = auth()->user();

            if($posts->user_id != $user->id)
            {
                return response()->json([
                    'success'   => false,
                    'message'   => 'Data post bukan milik user yang sedang login',
                ], 403);
            }

            $posts->delete();

            return response()->json([
                'success'   => true,
                'message'   => 'Data post berhasil di delete',
                'data'      => $posts,
            ], 200);
        }

        // return response()->json([
        //     'success'   => false,
        //     'message'   => 'Data post gagal di hapus',
        //     'data'      => $posts,
        // ], 404);
    }
}

