console.log("---------------")
console.log("Soal 1") //Soal 1
console.log("---------------")
//Jawaban
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];

daftarHewan.sort();
for(result of daftarHewan){
    console.log(result);
}
console.log("---------------")
console.log("Soal 2") //Soal 2
console.log("---------------")

//Jawaban
function introduce(data){
    var show = `Nama saya ${data.name}, umur saya ${data.age}, alamat saya di ${data.address}, dan saya punya hobby yaitu ${data.hobby}`
    return show
}
var data = {
    name        : "John",
    age         : 30,
    address     : "Jalan Pelesiran",
    hobby       : "Gaming",
}

var perkenalan = introduce(data)
console.log(perkenalan)

console.log("---------------")
console.log("Soal 3") //Soal 3
console.log("---------------")
//Jawaban
function hitung_huruf_vokal(hitung){
    var data = hitung.toLowerCase()
    var vokal = ['a', 'i', 'u', 'e', 'o']
    var input = []
    for(result of data){
        var find = vokal.indexOf(result)
        if(find >= 0){
            input.push(find)
        }
    }
    return input.length
}

var hitung1 = hitung_huruf_vokal("Muhammad")
var hitung2 = hitung_huruf_vokal("Iqbal")

console.log(hitung1, hitung2)

console.log("---------------")
console.log("Soal 4") //Soal 4
console.log("---------------")

//Jawaban
function hitung(data){
    return (2+((data -2)*2));
}

console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8
